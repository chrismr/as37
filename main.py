#!/usr/bin/env python3

from scripts import *
import math
import scipy.constants as consts
import matplotlib.pyplot as plt
from scipy import optimize as opt
from scipy.integrate import quad # Integration routine
import numpy as np

def main():
    """Main logic
    """
    xdata, ydata, zdata = import_data('SNLS.dat')
    data = (xdata, ydata, zdata)
    
    #linear_plot_logz_magnitude(data)
    #restricted_range(data)
    flat_universe(data)

def linear_plot_logz_magnitude(data):
    """Plots and saves the first plot of logz v. magnitude (with errors).
    """
    xdata, ydata, zdata = data
    xdata = np.array(xdata)
    
    # Plot original data with error bars
    plt.figure(0)
    plt.errorbar(xdata, ydata, yerr=zdata, fmt='.')
    
    # Plot a linear fit to original data
    popt, pocv = opt.curve_fit(linear_func, xdata, ydata, sigma=zdata)
    ymodel = popt[0]*xdata + popt[1]
    plt.plot(xdata, ymodel, 'r')
    chi_squared_min = chi_squared(ydata, ymodel, zdata)
    print("Linear fit: {}".format(popt))
    print(chi_squared_min)

    # Look for the error in the slope and plot the lines at 1 sigma
    chi_diff = get_chi_squared(get_linear, chi_squared_min + 1, data)
    err_slope = opt.fmin(chi_diff, 5, disp=False)
    err_fit = get_linear(err_slope)
    popt, pocv = opt.curve_fit(err_fit, xdata, ydata, sigma=zdata)
    ymodel = err_slope*xdata + popt
    err_chi2 = chi_squared(ydata, ymodel, zdata) 
    print("Error slope: %f, Chi2: %f" %(err_slope, err_chi2))
    plt.plot(xdata, ymodel, 'g')
    
    err_slope = opt.fmin(chi_diff, 6, disp=False)
    print("Error slope %f" %(err_slope))
    err_fit = get_linear(err_slope)
    popt, pocv = opt.curve_fit(err_fit, xdata, ydata, sigma=zdata)
    ymodel = err_slope*xdata + popt
    plt.plot(xdata, ymodel, 'g')
    
    # Make plot look nice and then save
    plt.title('Log(z) against Magnitude')
    plt.xlabel('Log_10 (Redshift)')
    plt.ylabel('Magnitude')
    plt.savefig('graphs/linear_logz_magnitude.png')
    plt.show()

def restricted_range(data):
    """Calculates a linear fit while restricting the range of data
    """
    # Get the data range that sets the confidence of 'the slope is 5' to 2 sigma
    index = len(data[0])
    chi2_difference = data_restrictor(6.18, data)
    min_diff = 0
    min_index = 0
    while index > 1:
        diff = chi2_difference(index)
        if diff < min_diff or min_diff == 0:
            min_diff = diff
            min_index = index
        index -= 1
    
    xdata, ydata, zdata = data
    x, y, z = np.array(xdata[:min_index]), ydata[:min_index], zdata[:min_index]
    
    # Make a nice graph with this range of data
    plt.figure(1)
    plt.errorbar(x, y, yerr=z, fmt='.')
    popt, pocv = opt.curve_fit(linear_func, x, y, sigma=z)
    ymodel = popt[0]*x + popt[1]
    chi_squared_min = chi_squared(y, ymodel, z)
    print("Reduced Linear fit: {}".format(popt))
    print("Reduced data set: ", min_index, chi2_difference(min_index))
    
    plt.plot(x, popt[0]*x + popt[1])
    
    plt.title('Log(z) against Magnitude (Data set of: %i points)' %(min_index))
    plt.xlabel('Log_10 (Redshift)')
    plt.ylabel('Magnitude')
    plt.savefig('graphs/restricted_linear_logz_magnitude.png')
    plt.show()

def flat_universe(data):
    """Logic for the flat universe model
    """
    xdata, ydata, zdata = data
    # Convert log(z) data to z
    for i in range(len(xdata)):
        xdata[i] = 10**xdata[i]
    
    # Fix FRW model with k=0, omegaT=1
    fit = flat_magnitude(0, 1)
    
    # Fit to this flat model
    popt, pocv = opt.curve_fit(fit, xdata, ydata)
    orig_fit_omegaM = popt[1]
    print("L, omegaM", popt)
    
    # Calculate chi2 for the fitted model and plot
    ymodel = [fit(x, popt[0], popt[1]) for x in xdata]
    chi_squared_min = chi_squared(ydata, ymodel, [1 for i in ydata])
    print(chi_squared_min)
    
    plt.figure(2)
    plt.errorbar(xdata, ydata, yerr=zdata, fmt='.')
    plt.plot(xdata, ymodel, 'r')
    
    # Tabulate the chi2 and plot them for various values of omegaM, searching for 1-sigma values
    min_omega = 0
    second_min = 0
    min_chi2_diff = np.inf
    min_chi2_diff2 = np.inf
    omegaM = np.linspace(0, 1, 500)
    chi2list = []
    for m in omegaM:
        popt, pocv = opt.curve_fit(get_get_flat(fit)(m), xdata, ydata)
        ymodel = [fit(x, popt, m) for x in xdata]
        chi2 = chi_squared(ydata, ymodel, [1 for i in xdata])
        chi2list.append(chi2)
        
        # Find the 1-sigma points
        if abs(chi2 - chi_squared_min - 1) < min_chi2_diff and m < orig_fit_omegaM:
            min_omega = m
            min_chi2_diff = abs(chi2 - chi_squared_min - 1)
        elif abs(chi2 - chi_squared_min - 1) < min_chi2_diff2 and m > orig_fit_omegaM:
            second_min = m
            min_chi2_diff2 = abs(chi2 - chi_squared_min - 1)
    
    # Fit to the magnitude with fixed omegaM at -1 sigma
    err_fit = get_get_flat(fit)(min_omega)
    popt, pocv = opt.curve_fit(err_fit, xdata, ydata)
    ymodel = [fit(x, popt, min_omega) for x in xdata]
    err_chi2 = chi_squared(ydata, ymodel, [1 for i in ydata]) 
    print("Error omegaM: %f, Chi2: %f" %(min_omega, err_chi2))
    plt.plot(xdata, ymodel, 'g')
    
    # Repeat at +1 sigma
    err_fit = get_get_flat(fit)(second_min)
    popt, pocv = opt.curve_fit(err_fit, xdata, ydata)
    ymodel = [fit(x, popt, second_min) for x in xdata]
    print("Error omegaM: %f" %(second_min))
    plt.plot(xdata, ymodel, 'g')
    
    plt.title('Redshift against Magnitude')
    plt.xlabel('Redshift: z')
    plt.ylabel('Magnitude')
    plt.savefig('graphs/flat.png')
    plt.show()
    
    # Fit Chi2 results to a quadratic
    plt.figure(3)
    plt.plot(omegaM, chi2list)
    popt, pocv = opt.curve_fit(quadratic, omegaM, chi2list)
    plt.plot(omegaM, [popt[0]*m**2 + popt[1]*m + popt[2] for m in omegaM], 'r')
    
    plt.title('Chi-squared values for fits to a fixed Omega_M')
    plt.xlabel('Omega_M')
    plt.ylabel('Chi-Squared')
    plt.savefig('graphs/flat-chi2.png')
    plt.show()
    
    print('Chi2 - Zero Cosmological constant: ', chi2list[-1])

def non_flat_universe(data):
    """Logic for the non-flat universe model
    """
    xdata, ydata, zdata = data
    # Convert log(z) data to z
    for i in range(len(xdata)):
        xdata[i] = 10**xdata[i]
    
    # Fix FRW model with k=0, omegaT=1
    fit = non_flat_magnitude()
    
    # Fit to this non-flat model
    popt, pocv = opt.curve_fit(fit, xdata, ydata)
    print("L, omegaM, omegaT, k", popt)
    
    # Calculate chi2 for the fitted model and plot
    ymodel = [fit(x, popt[0], popt[1]) for x in xdata]
    chi_squared_min = chi_squared(ydata, ymodel, [1 for i in ydata])
    print(chi_squared_min)
    
    plt.figure(4)
    plt.errorbar(xdata, ydata, yerr=zdata, fmt='.')
    plt.plot(xdata, ymodel, 'r')
    
    # Tabulate the chi2 and plot them for various values of omegaM, searching for 1-sigma values
    omegaM = np.linspace(0, 1, 100)
    omegaT = np.linspace(0, 2, 100)
    chi2list = [[]]
    for m in omegaM:
        column = []
        for t in omegaT:
            popt, pocv = opt.curve_fit(get_get_flat(fit)(m), xdata, ydata)
            ymodel = [fit(x, popt[0], m, t) for x in xdata]
            chi2 = chi_squared(ydata, ymodel, [1 for i in xdata])
            column.append(chi2)
        chi2list.append(column)
        
#        # Find the 1-sigma points
#        if abs(chi2 - chi_squared_min - 1) < min_chi2_diff and m < orig_fit_omegaM:
#            min_omega = m
#            min_chi2_diff = abs(chi2 - chi_squared_min - 1)
#        elif abs(chi2 - chi_squared_min - 1) < min_chi2_diff2 and m > orig_fit_omegaM:
#            second_min = m
#            min_chi2_diff2 = abs(chi2 - chi_squared_min - 1)
    
#    # Fit to the magnitude with fixed omegaM at -1 sigma
#    err_fit = get_get_flat(fit)(min_omega)
#    popt, pocv = opt.curve_fit(err_fit, xdata, ydata)
#    ymodel = [fit(x, popt, min_omega) for x in xdata]
#    err_chi2 = chi_squared(ydata, ymodel, [1 for i in ydata]) 
#    print("Error omegaM: %f, Chi2: %f" %(min_omega, err_chi2))
#    plt.plot(xdata, ymodel, 'g')
    
#    # Repeat at +1 sigma
#    err_fit = get_get_flat(fit)(second_min)
#    popt, pocv = opt.curve_fit(err_fit, xdata, ydata)
#    ymodel = [fit(x, popt, second_min) for x in xdata]
#    print("Error omegaM: %f" %(second_min))
#    plt.plot(xdata, ymodel, 'g')
    
    plt.title('Redshift against Magnitude')
    plt.xlabel('Redshift: z')
    plt.ylabel('Magnitude')
    plt.savefig('graphs/flat.png')
    plt.show()
    
#    # Fit Chi2 results to a quadratic
#    plt.figure(3)
#    plt.plot(omegaM, chi2list)
#    popt, pocv = opt.curve_fit(quadratic, omegaM, chi2list)
#    plt.plot(omegaM, [popt[0]*m**2 + popt[1]*m + popt[2] for m in omegaM], 'r')
#    
#    plt.title('Chi-squared values for fits to a fixed Omega_M')
#    plt.xlabel('Omega_M')
#    plt.ylabel('Chi-Squared')
#    plt.savefig('graphs/flat-chi2.png')
#    plt.show()


main()
