#!/usr/bin/env python3
# AS37 Scripts

import math
import scipy.constants as consts
import matplotlib.pyplot as plt
from scipy import optimize as opt
from scipy.integrate import quad # Integration routine
import numpy as np
#from scipy.stats import chisquare

H0 = 67 #km s^-1 Mpc^-1
S0 = 0#66.45742989 #71.19742576

def main():
    """Main logic
    """
    xdata, ydata, zdata = import_data('SNLS.dat')
    data = (xdata, ydata, zdata)
    
    linear_plot_logz_magnitude(data)
    #restricted_range(data)

def import_data(filename):
    """Import the data from the SNLS.dat file.
    """
    file = open(filename, 'r')
    xdata, ydata, zdata = [], [], []
    for i in list(file)[3:]:
        line = i.split()
        xdata.append(float(line[0]))
        ydata.append(float(line[1]))
        zdata.append(float(line[2]))
    return (xdata, ydata, zdata)

def linear_func(x, a, b):
    """returns y=ax+b
    """
    return a*x + b

def quadratic(x, a, b, c):
    return a*x**2 + b*x + c

def get_linear(a):
    """Returns a fixed slope linear function i.e. y(x,b)=ax+b
    """
    def aux_linear_fit(x, b):
        return a*x + b
    return aux_linear_fit

def chi_squared(data_values, model_values, sigma):
    """Calculate the value of the chi-squared statistic for the given data and
    model.
    """
    assert (len(data_values) == len(model_values)),'Unequal amount of data'
    chi_squared_sum = 0
    for i in range(len(data_values)):
        chi_squared_sum += ((data_values[i] - model_values[i]) / sigma[i]) ** 2
    return chi_squared_sum

def get_chi_squared(get_fit_func, target_chi, data):
    """Gets a function returning the squared difference in X2 values between a
    fixed parameter optimised fit, and some target value.
    """
    xdata, ydata, zdata = data
    xdata = np.array(xdata)
    def aux_chi_squared(frozen_param):
        fit_func = get_fit_func(frozen_param)
        popt, pocv = opt.curve_fit(fit_func, xdata, ydata, sigma=zdata)
        ymodel = [fit_func(x, popt) for x in xdata]
        return (target_chi - chi_squared(ydata, ymodel, zdata)) **2
    return aux_chi_squared

def recip_frw_h(z, omegaM, omegaT, omegaL):
    """Evaluates the value of 1/H(z) from the FRW equation using the densities
    """
    return (H0**2 * (omegaM * (1+z)**3 + (1-omegaT)*(1+z)**2 + omegaL))**-0.5

def bol_flux(k, L, z, omegaM, omegaT, omegaL):
    """Calculates the bolemetric flux from a redshifted object
    """
    r = actual_distance(z, k, omegaM, omegaT, omegaL)
    return L / (4 * math.pi * (1 + z)**2 * r**2)
    
def magnitude(k, L, z, omegaM, omegaT, omegaL):
    """Calculates the magnitude from the bolemetric flux (hence many parameters)
    """
    if not isinstance(z, float):
        return [(-2.5 * math.log10(bol_flux(k, L, zi, omegaM, omegaT, omegaL)) + S0) for zi in z]
    else:
        return -2.5 * math.log10(bol_flux(k, L, z, omegaM, omegaT, omegaL)) + S0

def flat_magnitude(k, omegaT):#, omegaL):
    """Returns a function to calculate the magnitude, with fixed values of k and omegaT
    """
    def aux_magnitude(z, L, omegaM):
        omegaL = omegaT - omegaM
        return magnitude(k, L, z, omegaM, omegaT, omegaL)
    return aux_magnitude

def get_get_flat(magnitude_func):
    """Passes the magnitude function to get_flat
    """
    def get_flat(omegaM):
        """Returns a function calculating the magnitude for a flat universe with
        omegaM as a frozen parameter
        """
        def aux_flat(z, L):
            return magnitude_func(z, L, omegaM)
        return aux_flat
    return get_flat

def non_flat_magnitude():#, omegaL):
    """Returns a function to calculate the magnitude with (currently) no parameters fixed
    """
    def aux_magnitude(z, L, omegaM, omegaT):
        k = 1 - omegaT
        omegaL = omegaT - omegaM
        return magnitude(k, L, z, omegaM, omegaT, omegaL)
    return aux_magnitude
    
def get_get_non_flat(magnitude_func):
    """Passes the magnitude function to get_non_flat
    """
    def get_non_flat(omegaM, omegaT, k):
        """Returns a function calculating the magnitude for a flat universe with
        omegaM as a frozen parameter
        """
        def aux_flat(z, L):
            return magnitude_func(z, L, omegaM, omegaT)
        return aux_flat
    return get_non_flat

def actual_distance(z, k, omegaM, omegaT, omegaL):
    """Calculates the comoving distance
    """
    integral = quad(recip_frw_h, 0, z, args=(omegaM,omegaT,omegaL))[0]
    #integral = np.array([quad(recip_frw_h, 0, zi, args=(omegaM,omegaT,omegaL))[0] for zi in z])
    #print("integral", integral)
    if k > 0:
        return (consts.c / H0 * (omegaT-1)**-0.5 * math.sin((omegaT-1)**0.5 * 
            H0 * integral))
    elif k == 0:
        return consts.c/ H0*integral
    else: # k < 0
        return (consts.c / H0 * (1 - omegaT)**-0.5 * 
            math.sinh((1 - omegaT)**0.5 * H0 * integral))

def data_restrictor(target_chi, data):
    """Returns a function that works out the difference in chi2 values
    between a free slope fit, and a fit to a fixed slope of 5,
    both for a reduced data set (data used up to the passed index).
    This is compared to a target chi2, with the squared difference returned
    """
    xdata, ydata, zdata = data
    def aux_chi_squared(index):
        index = int(index)
        x, y, z = np.array(xdata[:index]), ydata[:index], zdata[:index]
        popt, pocv = opt.curve_fit(linear_func, x, y, sigma=z)
        min_chi2 = chi_squared(y, popt[0]*x+popt[1], z)
        popt, pocv = opt.curve_fit(get_linear(5), x, y, sigma=z)
        return abs(target_chi -(chi_squared(y, 5*x + popt, zdata) - min_chi2))
    return aux_chi_squared





















